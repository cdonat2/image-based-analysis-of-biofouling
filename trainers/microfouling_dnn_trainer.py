#!/usr/bin/env python

"""
    Trainer and Callbacks for microfouling segmentation with convolutional neural networks.
"""

# import
from base.base_trainer import BaseTrain
import os
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard
from utils.config import create_dirs
import logging


class Trainer(BaseTrain):
    def __init__(self, model, data, config):
        super(Trainer, self).__init__(model, data, config)
        logging.info('Create trainer')
        self.callbacks = []
        self.loss = []
        self.acc = []
        self.val_loss = []
        self.val_acc = []
        self.epochs = config.trainer.num_epochs

        if config.exp.callback:
            create_dirs([config.callbacks.tensorboard_log_dir, config.callbacks.checkpoint_dir])
            self.init_callbacks()

    def init_callbacks(self):
        self.callbacks.append(
            ModelCheckpoint(
                filepath=os.path.join(self.config.callbacks.checkpoint_dir, '%s-{epoch:02d}-{val_loss:.2f}.hdf5' % self.config.exp.name),
                monitor=self.config.callbacks.checkpoint_monitor,
                mode=self.config.callbacks.checkpoint_mode,
                save_best_only=self.config.callbacks.checkpoint_save_best_only,
                save_weights_only=self.config.callbacks.checkpoint_save_weights_only,
                verbose=self.config.callbacks.checkpoint_verbose,
            )
        )

        self.callbacks.append(
            TensorBoard(
                log_dir = self.config.callbacks.tensorboard_log_dir,
                write_graph=self.config.callbacks.tensorboard_write_graph,
                profile_batch=0,
                update_freq='batch'
            )
        )

    def train(self):
        logging.info('Start training the model.')
        self.my_generator = self.data[0]
        self.validation_datagen = self.data[1]

        self.model.fit(
            self.my_generator,
            validation_data=self.validation_datagen,
            steps_per_epoch=50,
            validation_steps=50,
            epochs=self.epochs,
            callbacks=self.callbacks
        )


__author__ = 'Dr. Sreenivas Bhattiprolu'
__source__ = 'https://github.com/bnsreenu/python_for_microscopists/blob/master/216_mito_unet_12_training_images_V1.0.py'
