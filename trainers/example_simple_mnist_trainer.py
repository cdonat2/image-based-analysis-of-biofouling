#!/usr/bin/env python

"""
Trainer for Mnist example

Defines Trainer and Callbacks
"""

# import
from base.base_trainer import BaseTrain
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard
from utils.config import create_dirs
import os
import logging
import numpy as np
from sklearn.model_selection import KFold


class Trainer(BaseTrain):
    def __init__(self, model, data, config):
        super(Trainer, self).__init__(model, data, config)
        logging.info('Create trainer')
        self.model = model
        self.data = data
        self.config = config
        self.callbacks = []
        self.loss = []
        self.acc = []
        self.val_loss = []
        self.val_acc = []
        self.k = int(config.exp.k)

    def init_callbacks(self):
        self.callbacks.append(
            ModelCheckpoint(
                filepath=os.path.join(self.checkpoint_dir, '%s-{epoch:02d}-{val_loss:.2f}.hdf5' % self.config.exp.name),
                monitor=self.config.callbacks.checkpoint_monitor,
                mode=self.config.callbacks.checkpoint_mode,
                save_best_only=self.config.callbacks.checkpoint_save_best_only,
                save_weights_only=self.config.callbacks.checkpoint_save_weights_only,
                verbose=self.config.callbacks.checkpoint_verbose,
            )
        )

        self.callbacks.append(
            TensorBoard(
                log_dir = self.log_dir,
                write_graph=self.config.callbacks.tensorboard_write_graph,
                profile_batch=0
            )
        )

    def train(self):
        self.model.save_weights(os.path.join('utils', 'random_weights.h5'))

        logging.info('Start training the model.')
        self.X_train, self.y_train, self.X_test, self.y_test = self.data
        self.X = np.concatenate((self.X_train, self.X_test), axis=0)
        self.y = np.concatenate((self.y_train, self.y_test), axis=0)

        kf = KFold(n_splits=self.config.exp.k)
        fold_no = 1
        for train_index, test_index in kf.split(self.X, self.y):
            self.log_dir = os.path.join(self.config.callbacks.tensorboard_log_dir, 'fold_'+str(fold_no))
            self.checkpoint_dir = os.path.join(self.config.callbacks.checkpoint_dir, 'fold_'+str(fold_no))

            if self.config.exp.callback:
                create_dirs([self.log_dir, self.checkpoint_dir])
                self.init_callbacks()

            self.X_train, self.X_test = self.X[train_index], self.X[test_index]
            self.y_train, self.y_test = self.y[train_index], self.y[test_index]

            history = self.model.fit(
                self.X_train,
                self.y_train,
                epochs=self.config.trainer.num_epochs,
                verbose=self.config.trainer.verbose_training,
                batch_size=self.config.trainer.batch_size,
                validation_split=self.config.trainer.validation_split,
                callbacks=self.callbacks,
            )

            self.loss.extend(history.history['loss'])
            self.acc.extend(history.history['acc'])
            self.val_loss.extend(history.history['val_loss'])
            self.val_acc.extend(history.history['val_acc'])

            # Generate generalization metrics
            scores = self.model.evaluate(self.X[test_index], self.y[test_index], verbose=0)
            print(f'Score for fold {fold_no}: {self.model.metrics_names[0]} of {scores[0]}; {self.model.metrics_names[1]} of {scores[1]*100}%')
            self.model.load_weights(os.path.join('utils', 'random_weights.h5'))
            self.callbacks = []
            fold_no = fold_no + 1

__author__ = 'Ahmkel'
__source__ = 'https://github.com/Ahmkel/Keras-Project-Template'

