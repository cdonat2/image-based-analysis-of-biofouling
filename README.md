## Readme
Project for deep neural network based image analysis of biofouling growth patterns, see example:

Image             |  Mask        |  Prediction        
:-------------------------:|:-------------------------:|:-------------------------:
![image](utils/images/07_macrofouling_dnn2349_image.jpg)  |  ![image](utils/images/07_macrofouling_dnn2349_mask.png) | ![image](utils/images/07_macrofouling_dnn2349_prediction.png)

### Table of Contents
1. [Installation](#install)
2. [Project Structure](#projectstructure)
3. [Run experiments](#runexperiments)
4. [Tensorboard](#tensorboard)
5. [Sources](#sources)

### Installation <a name="install"></a>
1. Create a virtual environment for the project, for example in Conda.
   - Python 3.8 with tensorflow 2.5
2. Activate the environment in console.
3. Install requirements with:
    ```
    pip install -r requirements.txt
    ```

### Project Structure <a name="projectstructure"></a>
The project consists of several directories, which separate different stages of an experiment.
- 'base': base classes for data_loader, models, trainers, evaluators, and plotters
- 'configs' : configuration files for the experiments
- 'data_loader' : scripts for data acquisition and loading
- 'datasets' : datasets for training (only available locally)
- 'models' : model architectures
- 'evaluators': evaluation scripts
- 'documentation': related papers and books
- 'experiments': saved models and logs for experiments
- 'results': evaluations and predictions
- 'trainers': trainer models and set up for callbacks
- 'utils': utility functions, packages and examples

### Run experiments <a name="runexperiments"></a>
In order to run an experiment, several files need to be defined. This includes a:
- configuration file
- model class
- trainer class
- data_loader class
- evaluator class

Each set of individual files performs one experiment.

Run in terminal with:
```
python main.py -c [path to configuration file]
```

An example project is included:
```
python main.py -c configs/example_simple_mnist_config.json
```

### Tensorboard <a name="tensorboard"></a>
Tensorboard is an environment to show training progress for experiments. It can be opened in the browser with:
```
tensorboard --logdir=experiments/ --host localhost --port 8088
```

### Sources <a name="sources"></a>
[1] Project Structure: https://github.com/Ahmkel/Keras-Project-Template
