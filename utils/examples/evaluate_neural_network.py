#!/usr/bin/env python

"""
This Script evaluates a model create by neural_network.py

 Run this Script in terminal and the model created by neural_network.py in 'models' will be loaded. Select a number and a
    classification will be made as well as a plot
"""

# Imports
from tensorflow import keras

# Helper libraries
import matplotlib.pyplot as plt
import numpy as np

# Load Dataset
fashion_mnist = keras.datasets.fashion_mnist

# Prepare Data
(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()  # split into testing and training

test_images = test_images / 255.0

# Load Model
model = keras.models.load_model('../../models/saved/neural_network_model')

def predict(model, image, correct_label):
  class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']
  prediction = model.predict(np.array([image]))

  predicted_class = class_names[np.argmax(prediction)]
  print('Predicted: ', predicted_class)

  true_class = class_names[label]
  print('Truth: ', true_class)

  show_image(image, class_names[correct_label], predicted_class)


def show_image(img, label, guess):
  plt.figure()
  plt.imshow(img, cmap=plt.cm.binary)
  plt.title("Excpected: " + label)
  plt.xlabel("Guess: " + guess)
  plt.colorbar()
  plt.grid(False)
  plt.show()


def get_number():
  while True:
    num = input("Pick a number: ")
    if num.isdigit():
      num = int(num)
      if 0 <= num <= 1000:
        return int(num)
    else:
      print("Try again...")

COLOR = 'white'
plt.rcParams['text.color'] = COLOR
plt.rcParams['axes.labelcolor'] = COLOR

num = get_number()
image = test_images[num]
label = test_labels[num]
predict(model, image, label)

__author__ = 'Tech with Tim'
__source__ = 'https://colab.research.google.com/drive/1m2cg3D1x3j5vrFc-Cu0gMvc48gWyCOuG#forceEdit=true&sandboxMode=true'
