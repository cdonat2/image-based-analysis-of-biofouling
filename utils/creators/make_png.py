"""
This Script converts all images in a folder to .png
"""

from PIL import Image
import os
import tkinter as tk
from tkinter import filedialog

dir = filedialog.askdirectory(initialdir='results')

for file_name in os.listdir(dir):
    file_path = os.path.join(dir, file_name)
    img = Image.open(file_path)
    img_name = os.path.splitext(file_name)[0]
    file_name_new = img_name + '.png'
    file_path_new = os.path.join(dir, file_name_new)
    img.save(file_path_new)
