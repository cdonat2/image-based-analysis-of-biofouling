#!/usr/bin/env python

"""
Model for segmentation with transfer learning
"""

# import
from base.base_model import BaseModel
import segmentation_models as sm
import logging


class Model(BaseModel):
    def __init__(self, config):
        super(Model, self).__init__(config)
        self.size_y = config.model.img_height
        self.size_x = config.model.img_width
        self.build_model()

    def build_model(self):
        logging.info('Build model')
        backbone = 'resnet34'
        metric = sm.metrics.iou_score
        losses = sm.losses.bce_jaccard_loss
        # Define model
        self.model = sm.Unet(backbone, encoder_weights='imagenet', input_shape=(self.size_y, self.size_x, 3))
        self.model.compile('Adam', loss=losses, metrics=[metric])
        #print(self.model.summary())


__author__ = 'Google'
__source__ = 'https://www.tensorflow.org/tutorials/images/segmentation'
