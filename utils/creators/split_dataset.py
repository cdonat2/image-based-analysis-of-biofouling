'''
This script splits image folders in train, validation and testing folder
'''

#import
import splitfolders
from tkinter import filedialog

# Specify Path
dataset_dir = filedialog.askdirectory(initialdir='results')

splitfolders.ratio(dataset_dir, output=dataset_dir, seed=42, ratio=(0.90, 0.10))

__author__ = "Christian Donat"
__source__ = "https://pypi.org/project/split-folders/"