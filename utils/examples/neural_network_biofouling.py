#!/usr/bin/env python

"""
This Script is the first working neural network for classification of biofouling

This Script is a simple working example for taking images of biofouling and classifying those images.
It takes images from an IEEE Dataset and classifies it through a simple neural network, also it saves the created
model

Requires tensorflow 2.5
https://github.com/keras-team/keras-io/issues/12
"""

# Imports
import tensorflow as tf
from pathlib import Path
from tensorflow.keras import layers

# Load Data
data_dir = Path(__file__).parent / "..\\..\\datasets\\IEEEDataPort - Marine Fouling Images" # windows
#data_dir = Path(__file__).parent / "../../datasets/IEEEDataPort - Marine Fouling Images" # linux

# Set Parameters
batch_size = 32
img_height = 128
img_width = 128

# Create Training Data
train_ds = tf.keras.preprocessing.image_dataset_from_directory(
  data_dir,
  validation_split=0.2,
  subset="training",
  seed=123,
  image_size=(img_height, img_width),
  batch_size=batch_size)

# Create Validation Data
val_ds = tf.keras.preprocessing.image_dataset_from_directory(
  data_dir,
  validation_split=0.2,
  subset="validation",
  seed=123,
  image_size=(img_height, img_width),
  batch_size=batch_size)

class_names = train_ds.class_names
print(class_names)

for image_batch, labels_batch in train_ds:
  print(image_batch.shape)
  print(labels_batch.shape)
  break

# Prepare Data
normalization_layer = tf.keras.layers.experimental.preprocessing.Rescaling(1./255)

AUTOTUNE = tf.data.AUTOTUNE

train_ds = train_ds.cache().prefetch(buffer_size=AUTOTUNE)
val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)

# Train Model
num_classes = 10

model = tf.keras.Sequential([
  layers.experimental.preprocessing.Rescaling(1./255),
  layers.Conv2D(32, 3, activation='relu'),
  layers.MaxPooling2D(),
  layers.Conv2D(32, 3, activation='relu'),
  layers.MaxPooling2D(),
  layers.Conv2D(32, 3, activation='relu'),
  layers.MaxPooling2D(),
  layers.Flatten(),
  layers.Dense(128, activation='relu'),
  layers.Dense(num_classes)
])

model.compile(
  optimizer='adam',
  loss=tf.losses.SparseCategoricalCrossentropy(from_logits=True),
  metrics=['accuracy'])

model.fit(
  train_ds,
  validation_data=val_ds,
  epochs=10
)

# Save Model
#model.save('../../models/saved/neural_network_biofouling.hdf5') # linux
model.save('..\\..\\models\\saved\\neural_network_biofouling.hdf5') # windows

__author__ = 'Google'
__source__ = 'https://www.tensorflow.org/tutorials/load_data/images'