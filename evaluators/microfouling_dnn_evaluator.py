#!/usr/bin/env python

"""
    Evaluator for microfouling segmentation with convolutional neural networks.
"""

# import
from base.base_evaluator import BaseEval
import numpy as np
import logging


class Evaluator(BaseEval):
    def __init__(self, model, data, config):
        super(Evaluator, self).__init__(model, data, config)
        logging.info('Create evaluator')
        self.callbacks = []
        self.loss = []
        self.acc = []
        self.val_loss = []
        self.val_acc = []

    def eval(self, path_model):
        """
        Calculates IoU for the model

        Returns
        -------
        iou_score : float
            IoU Score
        """
        logging.info('Evaluate model')
        self.model.load_weights(path_model)

        self.X_test = self.data[2]
        self.y_test = self.data[3]

        # IOU
        y_pred = self.model.predict(self.X_test)
        y_pred_thresholded = y_pred > 0.5

        intersection = np.logical_and(self.y_test, y_pred_thresholded)
        union = np.logical_or(self.y_test, y_pred_thresholded)
        iou_score = np.sum(intersection) / np.sum(union)

        logging.info('IoU score of model is: {}'.format(iou_score))


__author__ = 'Dr. Sreenivas Bhattiprolu'
__source__ = 'https://github.com/bnsreenu/python_for_microscopists/blob/master/216_mito_unet_12_training_images_V1.0.py'
