#!/usr/bin/env python
"""
Calculates IoU of predictions with ground truth.
"""

import numpy as np
from matplotlib import pyplot as plt
import cv2
import glob
import os
from patchify import patchify
from patchify import unpatchify
import math
import logging
logging.basicConfig(level=logging.INFO)
import tkinter as tk
from tkinter import filedialog

logging.info('Funditec Predictions')

dir_images = filedialog.askdirectory(initialdir='results')
logging.info('Start predicting images from: {}'.format(dir_images))

dir_masks = os.path.join(dir_images, '..', 'masks')
dir_masks_funditec = os.path.join(dir_images, '..', 'masks_funditec')
dir_predictions = os.path.join(dir_images, '..', 'predictions_funditec')

# Check for directories
if not os.path.exists(dir_images):
    masks_exist = False
    logging.info("Image directory not found")
else:
    logging.info("Image directory found")
if not os.path.exists(dir_masks):
    logging.info("Mask directory not found")
else:
    logging.info("Mask directory found")
if not os.path.exists(dir_masks_funditec):
    logging.info("Funditec Mask directory not found")
else:
    logging.info("Funditec Mask directory found")
if not os.path.exists(dir_predictions):
    logging.info("Prediction directory not found")
    os.makedirs(dir_predictions)
    logging.info("Created empty directory predictions")
else:
    logging.info("Prediction directory found")

# Checking files inside of image directory
images_list = os.listdir(dir_images)
masks_list = os.listdir(dir_masks)

# Checking if the list is empty or not
if len(images_list) == 0:
    logging.info("Empty image directory")
    raise SystemExit(0)
elif len(masks_list) == 0:
    logging.info("Empty mask directory")
    masks_exist = False
else:
    logging.info("Found {} image(s)".format(len(images_list)))

# Checking for file type
for fname in os.listdir(dir_images):
    if not fname.endswith('.jpg'):
        logging.info("Found a file that is not .jpg in image directory: {}".format(fname))
        raise SystemExit(0)
for fname in os.listdir(dir_masks):
    if not fname.endswith('.jpg'):
        logging.info("Found a file that is not .jpg in mask directory: {}".format(fname))
        raise SystemExit(0)

for path_img in sorted(glob.glob(os.path.join(dir_images, '*.jpg'))):
    img_name = os.path.basename(path_img)
    logging.info('Predict: {}'.format(img_name))
    image_large = cv2.imread(path_img, 0)

    mask_name = os.path.join(os.path.splitext(img_name)[0] + '.jpg')
    logging.info('Mask Found: {}'.format(mask_name))
    mask_large = cv2.imread(os.path.join(dir_masks, mask_name), 0)
    mask_large = np.where(mask_large > 25, 255, 0)

    mask_funditec_name = os.path.join(os.path.splitext(img_name)[0] + '.jpg')
    logging.info('Funditec Mask Found: {}'.format(mask_funditec_name))
    mask_funditec_large = cv2.imread(os.path.join(dir_masks_funditec, mask_funditec_name), 0)
    mask_funditec_large = np.where(mask_funditec_large > 25, 255, 0)

    prediction_large = mask_funditec_large

    # Create Background and stack images
    image_large_y = image_large.shape[0]
    image_large_x = image_large.shape[1]
    canvas_y = int(math.floor(image_large_y / float(256))) * 256
    canvas_x = int(math.floor(image_large_x / float(256))) * 256

    image_cropped = image_large[0:canvas_y, 0:canvas_x]
    mask_cropped = mask_large[0:canvas_y, 0:canvas_x]
    prediction_cropped = prediction_large[0:canvas_y, 0:canvas_x]

    # Pixel Accuracy

    # IoU
    intersection = np.logical_and(mask_cropped, prediction_cropped)
    union = np.logical_or(mask_cropped, prediction_cropped)
    iou_score = np.sum(intersection) / np.sum(union)

    # Plot Results
    plt.figure(figsize=(12, 4))
    title = ['Input', 'Ground Truth', 'Prediction']
    plt.subplot(1, 3, 1)
    plt.title(title[0])
    plt.imshow(image_cropped, cmap='gray', interpolation='none')
    plt.axis('on')

    plt.subplot(1, 3, 2)
    plt.title(title[1])
    plt.imshow(mask_cropped)
    plt.axis('on')

    plt.subplot(1, 3, 3)
    plt.title(title[2])
    plt.imshow(image_large, cmap='gray', interpolation='none')
    plt.imshow(prediction_cropped, cmap='plasma', alpha=0.25)
    text = 'IoU Score:' + str(iou_score)
    plt.figtext(0.05, 0.00, text, fontsize=8, va="top", ha="left")
    plt.axis('on')
    # plt.show()

    path_prediction = os.path.join(dir_predictions, os.path.basename(path_img))
    logging.info('Finished: {}'.format(path_prediction))

    plt.savefig(path_prediction, bbox_inches="tight")


__author__ = 'Christian Donat'
__source__ = ''