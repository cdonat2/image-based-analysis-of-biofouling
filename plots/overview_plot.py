""" This Script creates barplots for overviews of performance for microfouling and macrofouling"""

# import
from base.base_plotter import BasePlotter

# Initialize Plotter
plotter = BasePlotter()

"""
Overview Plots
"""
# Microfouling
labels = ['K-Means', 'Weka ', 'DNN']
miou_scores = [0.6705, 0.7126, 0.8061]
pixel_accuracy_scores = [0.9226, 0.9489, 0.9730]

#plotter.scale = 1.5
#plotter.microfouling_overview(labels, miou_scores, 'mIoU', '07_microfouling_overview_miou')
#plotter.microfouling_overview(labels, pixel_accuracy_scores, 'Pixel Accuracy', '07_microfouling_overview_pa')

# Macrofouling
labels = ['K-Means', 'DNN23', 'DNN24', 'DNN234', 'DNN2349']
miou_scores = [0.3483, 0.3876, 0.4877, 0.5410, 0.6021]
pixel_accuracy_scores = [0.5775, 0.6709, 0.8243, 0.8938, 0.9514]

#plotter.scale = 2.0
#plotter.macrofouling_overview(labels, pixel_accuracy_scores, 'Pixel Accuracy', '07_macrofouling_overview_pa')
#plotter.scale = 1.25
#plotter.macrofouling_overview(labels, miou_scores, 'mIoU', '07_macrofouling_overview_miou')

# Macrofouling - Monthly Overview
labels = ['K-Means', 'M23', 'M24', 'M234', 'M2349']
kmeans_scores = [0.3086, 0.3046, 0.2588, 0.4037, 0.3780, 0.3441]
dnn23_scores = [0.3925, 0.3733, 0.3698, 0.4278, 0.3918, 0.3550]
dnn24_scores = [0.5038, 0.4857, 0.4432, 0.5211, 0.4725, 0.4880]
dnn234_scores = [0.5412, 0.5255, 0.5180, 0.5902, 0.5653, 0.5236]
dnn2349_scores = [0.5832, 0.5517, 0.5582, 0.6494, 0.6264, 0.5772]

plotter.plot_bar_chart_macrofouling_monthly(labels, kmeans_scores, dnn23_scores, dnn24_scores, dnn234_scores, dnn2349_scores, filename='07_macrofouling_monthly_overview')


__author__ = 'Christian Donat'
__source__ = ''
