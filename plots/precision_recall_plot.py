""" This Script creates barplots for precision/recall for microfouling and macrofouling"""

# import
from base.base_plotter import BasePlotter

# Initialize Plotter
plotter = BasePlotter()

"""
Precision / Recall
"""

# Microfouling
labels = ['Precision', 'Recall']
kmeans_precision_scores = [0.4835, 0.9830]  # [Biofouling, Background]
kmeans_recall_scores = [0.7960, 0.9326]
weka_precision_scores = [0.6246, 0.9807]
weka_recall_scores = [0.7608, 0.9638]
dnn_precision_scores = [0.8808, 0.9789]
dnn_recall_scores = [0.7306, 0.9922]

plotter.scale = 1.5
#plotter.precision_recall(labels, kmeans_precision_scores, kmeans_recall_scores, filename='07_microfouling_kmeans_precision_recall')
plotter.precision_recall(labels, weka_precision_scores, weka_recall_scores, filename='07_microfouling_weka_precision_recall')
plotter.precision_recall(labels, dnn_precision_scores, dnn_recall_scores, filename='07_microfouling_dnn_precision_recall')

# Macrofouling
labels = ['Precision', 'Recall']
kmeans_precision_scores = [0.7317, 0.3736]  # [Biofouling, Background]
kmeans_recall_scores = [0.6069, 0.5131]

dnn23_precision_scores = [0.9747, 0.4869]
dnn23_recall_scores = [0.5350, 0.9693]

dnn24_precision_scores = [0.9815, 0.6462]
dnn24_recall_scores = [0.7586, 0.9685]

dnn234_precision_scores = [0.9735, 0.7672]
dnn234_recall_scores = [0.8691, 0.9480]

dnn2349_precision_scores = [0.9786, 0.8970]
dnn2349_recall_scores = [0.9501, 0.9544]

plotter.y_range = 1.5
#plotter.precision_recall(labels, kmeans_precision_scores, kmeans_recall_scores, filename='07_macrofouling_kmeans_precision_recall')
#plotter.precision_recall(labels, dnn24_precision_scores, dnn24_recall_scores, filename='07_macrofouling_dnn24_precision_recall')
plotter.precision_recall(labels, dnn23_precision_scores, dnn23_recall_scores, filename='07_macrofouling_dnn23_precision_recall')
#plotter.precision_recall(labels, dnn234_precision_scores, dnn234_recall_scores, filename='07_macrofouling_dnn234_precision_recall')
#plotter.precision_recall(labels, dnn2349_precision_scores, dnn2349_recall_scores, filename='07_macrofouling_dnn2349_precision_recall')


__author__ = 'Christian Donat'
__source__ = ''
