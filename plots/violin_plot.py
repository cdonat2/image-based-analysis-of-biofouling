""" This Script creates violin plots for microfouling and macrofouling"""

# import
from base.base_plotter import BasePlotter
import pandas as pd

# Initialize Plotter
plotter = BasePlotter()

"""
Violin Plot
"""

# Microfouling - 256 patches

# Create Dataset
path_kmeans = '../results/01 Microfouling/microfouling_kmeans/2021-12-20/log.txt'
path_weka = '../results/01 Microfouling/microfouling_weka/2021-12-20/log.txt'
path_dnn = '../results/01 Microfouling/microfouling_dnn/2021-12-20/log.txt'

df_kmeans = plotter.create_dataframe(path_kmeans, experiment='kmeans')
df_weka = plotter.create_dataframe(path_weka, experiment='weka')
df_dnn = plotter.create_dataframe(path_dnn, experiment='dnn')
df_micro = pd.concat([df_kmeans, df_weka, df_dnn])

labels = ['K-Means', 'Weka', 'DNN']
#plotter.plot_violin_vertical(labels, df_micro, 'Measurement Error', '07_microfouling_bias_overview')


# Macrofouling

# Create Dataset
# K-Means
df_kmeans_m5 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_kmeans/M5 DTIF-Fouling Study (13th August 2020)/2021-12-14/log.txt', experiment='kmeans')
df_kmeans_m7 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_kmeans/M7 DTIF-Fouling Study (8th October 2020)/2021-12-14/log.txt', experiment='kmeans')
df_kmeans_m8 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_kmeans/M8 DTIF-Fouling Study (Date Collected_ 23rd November 2020)/2021-12-14/log.txt', experiment='kmeans')
df_kmeans_m10 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_kmeans/M10 DTIF-Fouling Study/2021-12-14/log.txt', experiment='kmeans')
df_kmeans_m11 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_kmeans/M11 DTIF-Fouling Study 19th Feb 2021/2021-12-14/log.txt', experiment='kmeans')
df_kmeans_m12 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_kmeans/M12 DTIF-Fouling Study/2021-12-14/log.txt', experiment='kmeans')
df_kmeans = pd.concat([df_kmeans_m5, df_kmeans_m7, df_kmeans_m8, df_kmeans_m10, df_kmeans_m11, df_kmeans_m12])

# DNN 23
df_dnn23_m5 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_23_090/M5 DTIF-Fouling Study (Date collected_13th August 2020)/log.txt', experiment='dnn23')
df_dnn23_m7 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_23_090/M7 DTIF-Fouling Study (Date collected_8th October 2020)/log.txt', experiment='dnn23')
df_dnn23_m8 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_23_090/M8 DTIF-Fouling Study (Date Collected_ 23rd November 2020)/log.txt', experiment='dnn23')
df_dnn23_m10 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_23_090/M10 DTIF-Fouling Study/log.txt', experiment='dnn23')
df_dnn23_m11 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_23_090/M11 DTIF-Fouling Study (Date collected_19th Feb 2021)/log.txt', experiment='dnn23')
df_dnn23_m12 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_23_090/M12 DTIF-Fouling Study/log.txt', experiment='dnn23')
df_dnn23 = pd.concat([df_dnn23_m5, df_dnn23_m7, df_dnn23_m8, df_dnn23_m10, df_dnn23_m11, df_dnn23_m12])

# DNN 24
df_dnn24_m5 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_24/M5 DTIF-Fouling Study (13th August 2020)/2021-12-14/log.txt', experiment='dnn24')
df_dnn24_m7 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_24/M7 DTIF-Fouling Study (8th October 2020)/2021-12-14/log.txt', experiment='dnn24')
df_dnn24_m8 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_24/M8 DTIF-Fouling Study (Date Collected_ 23rd November 2020)/2021-12-14/log.txt', experiment='dnn24')
df_dnn24_m10 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_24/M10 DTIF-Fouling Study/2021-12-14/log.txt', experiment='dnn24')
df_dnn24_m11 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_24/M11 DTIF-Fouling Study 19th Feb 2021/2021-12-14/log.txt', experiment='dnn24')
df_dnn24_m12 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_24/M12 DTIF-Fouling Study/2021-12-14/log.txt', experiment='dnn24')
df_dnn24 = pd.concat([df_dnn24_m5, df_dnn24_m7, df_dnn24_m8, df_dnn24_m10, df_dnn24_m11, df_dnn24_m12])

# DNN 234
df_dnn234_m5 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_234/M5 DTIF-Fouling Study (13th August 2020)/2021-12-14/log.txt', experiment='dnn234')
df_dnn234_m7 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_234/M7 DTIF-Fouling Study (8th October 2020)/2021-12-14/log.txt', experiment='dnn234')
df_dnn234_m8 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_234/M8 DTIF-Fouling Study (Date Collected_ 23rd November 2020)/2021-12-14/log.txt', experiment='dnn234')
df_dnn234_m10 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_234/M10 DTIF-Fouling Study/2021-12-14/log.txt', experiment='dnn234')
df_dnn234_m11 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_234/M11 DTIF-Fouling Study 19th Feb 2021/2021-12-14/log.txt', experiment='dnn234')
df_dnn234_m12 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_234/M12 DTIF-Fouling Study/2021-12-14/log.txt', experiment='dnn234')
df_dnn234 = pd.concat([df_dnn234_m5, df_dnn234_m7, df_dnn234_m8, df_dnn234_m10, df_dnn234_m11, df_dnn234_m12])

# DNN 2349
df_dnn2349_m5 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_2349/M5 DTIF-Fouling Study (13th August 2020)/2021-12-14/log.txt', experiment='dnn2349')
df_dnn2349_m7 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_2349/M7 DTIF-Fouling Study (8th October 2020)/2021-12-14/log.txt', experiment='dnn2349')
df_dnn2349_m8 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_2349/M8 DTIF-Fouling Study (Date Collected_ 23rd November 2020)/2021-12-14/log.txt', experiment='dnn2349')
df_dnn2349_m10 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_2349/M10 DTIF-Fouling Study/2021-12-14/log.txt', experiment='dnn2349')
df_dnn2349_m11 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_2349/M11 DTIF-Fouling Study 19th Feb 2021/2021-12-14/log.txt', experiment='dnn2349')
df_dnn2349_m12 = plotter.create_dataframe('../results/02 Macrofouling/macrofouling_month_2349/M12 DTIF-Fouling Study/2021-12-14/log.txt', experiment='dnn2349')
df_dnn2349 = pd.concat([df_dnn2349_m5, df_dnn2349_m7, df_dnn2349_m8, df_dnn2349_m10, df_dnn2349_m11, df_dnn2349_m12])

# Complete Dataframe
df_macro = pd.concat([df_kmeans, df_dnn23, df_dnn24, df_dnn234, df_dnn2349])

labels = ['K-Means', 'DNN23', 'DNN24', 'DNN234', 'DNN2349']
#plotter.plot_violin_vertical(labels, df_macro, 'Measurement Error', '07_macrofouling_bias_overview')

labels = ['DNN', 'Weka', 'K-Means']
order = ['dnn', 'weka', 'kmeans']
plotter.y_range = [-0.4, 3]
plotter.x_range = [-0.3, 0.3]
#plotter.plot_violing_horizontal(order, labels, df_micro, 'Microfouling Prediction Bias', '07_microfouling_prediction_bias')

labels = ['DNN2349', 'DNN234', 'DNN24', 'DNN23', 'K-Means']
order=['dnn2349', 'dnn234', 'dnn24', 'dnn23', 'kmeans']
plotter.y_range = [-0.4, 5]
plotter.x_range = [-1.1, 1.1]

plotter.plot_violing_horizontal(order, labels, df_macro, 'Macrofouling Prediction Bias', '07_macrofouling_prediction_bias')

__author__ = 'Christian Donat'
__source__ = ''
