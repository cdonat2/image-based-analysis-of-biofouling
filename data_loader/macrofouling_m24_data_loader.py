#!/usr/bin/env python

"""
Data loader for macrofouling semantic segmentation with transfer learning.

Takes complete months 2,4 for training.
"""

# import
import tensorflow as tf
import segmentation_models as sm
from base.base_data_loader import BaseDataLoader
import os
import numpy as np
import matplotlib.pyplot as plt
import logging


class DataLoader(BaseDataLoader):
    def __init__(self, config):
        super(DataLoader, self).__init__(config)
        logging.info('Create data loader')
        self.size_y = config.model.img_height
        self.size_x = config.model.img_width
        self.n_classes = config.model.num_cls
        self.seed = config.exp.seed
        self.environment = 'windows'
        sm.set_framework('tf.keras')
        sm.framework()

        def parse_image(img_path: str) -> dict:
            """Load an image and its mask and returns."""
            img = tf.io.read_file(img_path)
            img = tf.image.decode_jpeg(img, channels=3)
            img = tf.image.convert_image_dtype(img, tf.uint8)
            img = tf.image.resize(img, [self.size_x, self.size_y], method='nearest')

            mask_path = tf.strings.regex_replace(img_path, "images", "masks") # Replace images with masks in string
            mask_path = tf.strings.regex_replace(mask_path, "jpg", "png") # Replace jpg with png in string
            mask = tf.io.read_file(mask_path)
            # The masks contain a class index for each pixels
            mask = tf.image.decode_png(mask, channels=1)
            mask = tf.where(mask > 1, np.dtype('uint8').type(1), mask)  # Every Value bigger then 0 is considered Biofouling
            mask = tf.image.resize(mask, [self.size_x, self.size_y], method='nearest')
            return {'image': img, 'segmentation_mask': mask}

        @tf.function
        def normalize(input_image: tf.Tensor, input_mask: tf.Tensor) -> tuple:
            """Rescale the pixel values of the images between 0.0 and 1.0 compared to [0,255] originally."""
            input_image = tf.cast(input_image, tf.float32) / 255.0
            return input_image, input_mask

        @tf.function
        def load_image(datapoint: dict) -> tuple:
            """Apply some transformations to an input dictionary containing a train image and its annotation."""
            input_image = tf.image.resize(datapoint['image'], (self.size_x, self.size_y))
            input_mask = tf.image.resize(datapoint['segmentation_mask'], (self.size_x, self.size_y))
            backbone = 'resnet34'
            preprocess_input = sm.get_preprocessing(backbone)
            # Preprocess input
            input_image = preprocess_input(input_image)
            input_mask = preprocess_input(input_mask)
            input_image, input_mask = normalize(input_image, input_mask)
            return input_image, input_mask

        def display_sample(display_list):
            """Show side-by-side an input image, the ground truth and the prediction."""
            plt.figure(figsize=(12, 12))
            title = ['Input Image', 'True Mask', 'Predicted Mask']
            for i in range(len(display_list)):
                plt.subplot(1, len(display_list), i+1)
                plt.title(title[i])
                plt.imshow(tf.keras.preprocessing.image.array_to_img(display_list[i]))
                plt.axis('off')
            plt.show()

        # Create Dataset
        dir_images = os.path.join('datasets',
                                  '01 Dublin',
                                  '02 Macrofouling',
                                  '02 Preprocessed',
                                  'M24',
                                  'training and validation',
                                  'images/')

        # Windows
        if os.path.isdir(dir_images):
            logging.info('Windows Environment')
        # Google Colab
        else:
            logging.info('Google Environment')
            self.environment = 'google'
            dir_images = os.path.join('..', dir_images)

        size_dataset = len(os.listdir(dir_images))
        logging.info('The Dataset contains {} images.'.format(size_dataset))

        autotune = tf.data.experimental.AUTOTUNE
        # https://stackoverflow.com/questions/46444018/meaning-of-buffer-size-in-dataset-map-dataset-prefetch-and-dataset-shuffle
        buffer_size = 1000

        self.dataset = tf.data.Dataset.list_files(os.path.join(dir_images, '*.jpg'), seed=self.seed)
        self.dataset.shuffle(buffer_size=buffer_size, seed=self.seed)
        self.dataset = self.dataset.map(parse_image)

        # Split Dataset into training, validation and test
        config.trainer.training_size = int((1-config.trainer.validation_split) * size_dataset)
        config.trainer.validation_size = int(config.trainer.validation_split * size_dataset)

        logging.info('Taking {} images for training.'.format(config.trainer.training_size))
        logging.info('Taking {} images for validation.'.format(config.trainer.validation_size))

        dataset_train = self.dataset.take(config.trainer.training_size)
        dataset_val = self.dataset.skip(config.trainer.training_size)
        self.dataset = {"train": dataset_train, "val": dataset_val}

        # Train Dataset
        self.dataset['train'] = self.dataset['train'].map(load_image, num_parallel_calls=tf.data.experimental.AUTOTUNE)
        # how shuffle works: https://stackoverflow.com/a/53517848
        self.dataset['train'] = self.dataset['train'].shuffle(buffer_size=buffer_size, seed=self.seed)
        self.dataset['train'] = self.dataset['train'].repeat()
        self.dataset['train'] = self.dataset['train'].batch(config.trainer.batch_size)
        self.dataset['train'] = self.dataset['train'].prefetch(buffer_size=autotune)

        # Validation Dataset
        self.dataset['val'] = self.dataset['val'].map(load_image)
        self.dataset['val'] = self.dataset['val'].repeat()
        self.dataset['val'] = self.dataset['val'].batch(config.trainer.batch_size)
        self.dataset['val'] = self.dataset['val'].prefetch(buffer_size=autotune)
        self.data = self.dataset

    def get_data(self):
        return self.data


__author__ = 'Adapted from Yann Le Guilly'
__source__ = 'https://yann-leguilly.gitlab.io/post/2019-12-14-tensorflow-tfdata-segmentation/'
