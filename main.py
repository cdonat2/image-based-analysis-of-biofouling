#!/usr/bin/env python

"""
Main file for experiments

This file connects all parts of the experiment: data_loader, model, trainer, and evaluator.
"""

from utils.config import process_config, get_args
import logging
import importlib


def main():
    # Capture the config path from the run arguments, then process the json configuration file
    try:
        args = get_args()
        config = process_config(args.config)
    except:
        logging.warning('Arguments missing or invalid')
        exit(0)

    # Import modules
    module_loader = importlib.import_module('data_loader.' + config.exp.module_loader)
    module_model = importlib.import_module('models.' + config.exp.module_model)
    module_trainer = importlib.import_module('trainers.' + config.exp.module_trainer)
    module_evaluator = importlib.import_module('evaluators.' + config.exp.module_evaluator)

    # Create Data Loader
    data_loader = module_loader.DataLoader(config)

    # Create Model
    model = module_model.Model(config)

    # Train
    if config.exp.train:
        trainer = module_trainer.Trainer(model.model, data_loader.get_data(), config)
        trainer.train()

    # Evaluate
    evaluator = module_evaluator.Evaluator(model.model, data_loader.get_data(), config)
    if config.exp.evaluate:
        evaluator.eval()

    # Predict
    if config.exp.predict:
        evaluator.predict_images(type='grayscale')


if __name__ == '__main__':
    main()

__author__ = 'Ahmkel'
__source__ = 'https://github.com/Ahmkel/Keras-Project-Template'

