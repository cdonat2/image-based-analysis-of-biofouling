#!/usr/bin/env python

"""
Evaluator for Mnist Example
"""

# import
from base.base_evaluator import BaseEval
from tensorflow.keras.datasets import mnist
import logging


class Evaluator(BaseEval):
    def __init__(self, model, data, config):
        super(Evaluator, self).__init__(model, data, config)
        logging.info('Create evaluator')
        self.model = model
        self.data = data
        self.config = config
        self.callbacks = []
        self.loss = []
        self.acc = []
        self.val_loss = []
        self.val_acc = []

    def eval(self):
        """ Evaluates Model """
        logging.info('Evaluate model')
        (self.X_train, self.y_train), (self.X_test, self.y_test) = mnist.load_data()
        self.X_train = self.X_train.reshape((-1, 28 * 28))
        self.X_test = self.X_test.reshape((-1, 28 * 28))

        # Evaluate
        test_loss, test_acc = self.model.evaluate(self.X_test, self.y_test, verbose=1)
        logging.info('Test accuracy: {}'.format(test_acc))

    def predict(self):
        logging.info('Predict images - not implemented')


__author__ = 'Christian Donat'
__source__ = ''
