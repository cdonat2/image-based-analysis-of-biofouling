#!/usr/bin/env python

"""
Evaluation of Weka Segmentation

This script performs the evaluation of segmentation masks created by the weka segmentation tool.

The user can select the "images" directory through a file-dialog by running this script. In addition, the segmentation
results are evaluated. A log file with the performance is created as well.

How to Run:
    python weka_evaluator.py
"""

# import
from base.base_evaluator import BaseEval
import cv2
from PIL import Image
import numpy as np
import os
from base.logger import Logger
import logging


class WekaEvaluator(BaseEval):
    """ This class implements evaluation methods for predictions made by the Weka segmentation tool.

    Methods:
    def load_prediction(self, img_name):
        load a prediction
    def predict_images():
        calculate metrics and create log for predictions

    """
    def __init__(self):
        """ Initialize WekaEvaluator.

        Initializes attributes and checks if all necessary directories are present. Does not inherit __init from
        BaseEval().
        """
        logging.getLogger().setLevel(logging.INFO)
        logging.info('Create evaluator')
        self.time_elapsed = 0.

        self.select_img_dir()
        self.img_dir_path = self.check_dir('images')
        self.masks_dir_path = self.check_dir('masks')
        self.predictions_dir_path = self.check_dir('predictions')
        self.predictions_overlay_dir_path = self.check_dir('predictions overlay')
        self.check_masks()

    def load_prediction(self, img_name):
        """Load prediction from prediction directory.

        :param img_name: (string) name of a prediction
        :return:
            img_large: (ndarray) prediction
            img_name: (string) prediction name
        """
        prd_name = img_name
        prd_filename = prd_name + '.png'
        prd_path = os.path.join(self.predictions_dir_path, prd_filename)
        prd_large = cv2.imread(prd_path, 0)
        prd_large = np.where(prd_large > 25, 255, 0)  # binarize
        if np.max(prd_large) > 0:
            prd_large = prd_large / np.max(prd_large)  # normalize
        prd_large = Image.fromarray(prd_large)
        prd_large = np.array(prd_large)
        return prd_large, prd_name

    def evaluate_predictions(self):
        """Evaluate predictions for images.

        :param
        :return:
        """
        logging.info('Evaluate images/predictions for: {}'.format(self.img_dir_path))
        # Create Log File
        Logger.create_log_file(self)
        # Iterate through image folder
        for idx, img_filename in enumerate(os.listdir(self.img_dir_path), start=1):  # For all images in folder
            logging.info('Evaluate: {} / ({}/{})'.format(img_filename, idx, len(os.listdir(self.img_dir_path))))
            # Load image, mask
            img_name = os.path.splitext(img_filename)[0]
            img, img_name = self.load_image(img_name)
            msk, msk_name = self.load_mask(img_name)
            # Load prediction
            prd, prd_name = self.load_prediction(img_name)

            # Calculate Metrics
            metrics = [0] * 5
            if self.masks_exist:
                iou = self.calculate_iou(msk, prd)
                cvg_true = self.calculate_coverage(msk)
                cvg_predicted = self.calculate_coverage(prd)
                cvg_error = abs(cvg_predicted - cvg_true)
                pixel_accuracy = self.calculate_pixel_accuracy(msk, prd)
                pixel_counts = self.calculate_pixel_count(msk, prd)
                metrics = [iou, pixel_accuracy, cvg_true, cvg_predicted, cvg_error, pixel_counts]

            # Create Plot
            self.plot(img, msk, prd, img_filename, metrics)
            # Write log-file
            Logger.write_log_line(self, img_filename, metrics)


# Run script
evaluator = WekaEvaluator()
evaluator.evaluate_predictions()

__author__ = 'Christian Donat'
__source__ = ''
