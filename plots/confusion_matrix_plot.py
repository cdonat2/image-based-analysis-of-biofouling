""" This Script creates confusion matrices for microfouling and macrofouling"""

# import
from base.base_plotter import BasePlotter
import numpy as np

# Initialize Plotter
plotter = BasePlotter()

"""
Confusion Matrix 
"""

# Microfouling
microfouling_counts_kmean = np.array([[3829, 4091], [981, 56635]])
microfouling_counts_weka = np.array([[3660, 2200], [1150, 58526]])
microfouling_counts_dnn = np.array([[3514, 475], [1296, 60251]])

plotter.scale = 256*256
#plotter.plot_confusion_matrix(microfouling_counts_weka, title='Confusion Matrix', filename='07_microfouling_weka_confusion_matrix')
#plotter.plot_confusion_matrix(microfouling_counts_kmean, title='Confusion Matrix', filename='07_microfouling_kmeans_confusion_matrix')
#plotter.plot_confusion_matrix(microfouling_counts_dnn, title='Confusion Matrix', filename='07_microfouling_dnn_confusion_matrix')

# Macrofouling
macrofouling_counts_kmean = np.array([[416555, 152718], [269796, 160931]])
macrofouling_counts_dnn24 = np.array([[34162, 645], [10872, 19857]])
macrofouling_counts_dnn234 = np.array([[39138, 1067], [5896, 19435]])
macrofouling_counts_dnn2349 = np.array([[42786, 935], [2248, 19567]])
macrofouling_counts_dnn23 = np.array([[24094, 628], [20940, 19873]])

# Create Confusion Matrix
plotter.scale = 1000*1000
#plotter.plot_confusion_matrix(macrofouling_counts_kmean, title='Confusion Matrix', filename='07_macrofouling_kmeans_confusion_matrix')

plotter.scale = 256*256
#plotter.plot_confusion_matrix(macrofouling_counts_dnn24, title='Confusion Matrix', filename='07_macrofouling_dnn24_confusion_matrix')
#plotter.plot_confusion_matrix(macrofouling_counts_dnn234, title='Confusion Matrix', filename='07_macrofouling_dnn234_confusion_matrix')
#plotter.plot_confusion_matrix(macrofouling_counts_dnn2349, title='Confusion Matrix', filename='07_macrofouling_dnn2349_confusion_matrix')
plotter.plot_confusion_matrix(macrofouling_counts_dnn23, title='Confusion Matrix', filename='07_macrofouling_dnn23_confusion_matrix')

# Example in Background Chapter
background = 180 * 180
object = 130 * 106
prediction = 127 * 115
intersection = 112 * 83

true_neg = background - object - prediction + intersection
false_neg = object - intersection
true_pos = intersection
false_pos = prediction - intersection

metrics_example = np.array([[true_pos, false_pos], [false_neg, true_neg]])

plotter.scale = 180*180
#plotter.plot_confusion_matrix(metrics_example, title='Confusion Matrix', filename='03_metrics_matrix')


__author__ = 'Christian Donat'
__source__ = ''
