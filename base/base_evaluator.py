#!/usr/bin/env python

"""
Base class for Evaluator

This module implements the basic evaluator class.

This module contains basic methods to load images, masks, create directories for predictions, evaluate models and plot
visualizations.

Classes:
    BaseEval
"""

# import
import numpy as np
import cv2
import os
import math
import tkinter as tk
import logging
import time
from tkinter import filedialog
from patchify import patchify
from patchify import unpatchify
from matplotlib import pyplot as plt
from PIL import Image
from base.logger import Logger


class BaseEval(object):
    """ This class implements the basic methods for evaluation.

    Attributes:
          model (model) trained .hdf5 model
          data (dataset) dataset which was used for training
          config (file) config information

    Methods:
        eval(self)
            evaluate a trained model with a dataset
        select_img_dir(self)
            select path for image directory
        check_dir(self, dir_name)
            check if a directory with name "dir_name" exists
        check_masks(self)
            check if masks exist for predictions
        load_image(self, img_filename):
            load individual image from image directory
        load_mask(self, img_name):
            load individual mask from image directory and create placeholder if no mask is found
        load_weights(self):
            load model weights
        pad_image(self, img_large):
            pad image
        predict_images(self, type):
            predict images in a folder
        predict_img_greyscale(self, img):
            predict single grayscale image
        predict_img_color(self, img):
            predict single color image
        calculate_iou(self, mask, prediction):
            calculate Intersection over Union
        calculate_coverage(self, image):
            calculate coverage
        calculate_pixel_accuracy(self, mask, prediction):
            calculate Pixel Accuracy
        save_prediction(self, prediction, dir_predictions, file_name):
            save a prediction
        plot(self, image_left, mask_large, prediction_large, file_name, metrics):
            plot an image with its prediction
    """

    def __init__(self, model, data, config):
        """Initialize BaseEval.

        Initializes attributes and checks if all necessary directories are present when predictions are to be made.
        """
        self.model = model
        self.data = data
        self.config = config
        self.n_classes = config.model.num_cls
        self.patch_size = config.model.img_height
        self.time_elapsed = 0
        self.prediction_size_x = config.exp.prediction_width
        self.prediction_size_y = config.exp.prediction_height
        self.masks_exist = False

        if self.config.exp.evaluate or self.config.exp.predict:
            self.load_weights()

        # Check Directories
        if self.config.exp.predict:
            self.select_img_dir()
            self.images_dir_path = self.check_dir('images')
            self.masks_dir_path = self.check_dir('masks')
            self.predictions_dir_path = self.check_dir('predictions')
            self.predictions_overlay_dir_path = self.check_dir('predictions overlay')
            self.check_masks()

    def eval(self):
        raise NotImplementedError

    def select_img_dir(self):
        """Create path for image directory."""
        logging.info('Select the image directory to predict')
        self.img_dir_path = filedialog.askdirectory(initialdir='results')

    def check_dir(self, dir_name):
        """Check for directory with dir_name on same depth as image directory.

        Directory is created if it is missing.
        :param dir_name: (string) name of checked directory
        :return:
            dir_path: (string) path of checked directory
        """
        dir_path = os.path.join(self.img_dir_path, '..', dir_name)
        if not os.path.exists(dir_path):
            logging.info(dir_name + " directory not found")
            os.makedirs(dir_path)
            logging.info("Created empty directory " + dir_name)
        else:
            logging.info(dir_name + " directory found")
            n_elements = len(os.listdir(dir_path))
            if n_elements == 0:
                logging.info("directory empty")
            else:
                logging.info("{} element(s) found".format(n_elements))
        return dir_path

    def check_masks(self):
        """Check if masks in mask_dir exist."""
        if len(os.listdir(self.masks_dir_path)) != 0:  # check for elements in mask directory
            self.masks_exist = True
        else:
            self.masks_exist = False

    def load_image(self, img_name):
        """Load image from image directory.

        :param img_filename: (string) filename of an image
        :return:
            img_large: (ndarray) image
            img_name: (string) image name
        """
        img_filename = img_name + '.jpg'
        img_path = os.path.join(self.img_dir_path, img_filename)
        img_large = cv2.imread(img_path, 1)
        img_large = Image.fromarray(img_large)
        img_large = np.array(img_large)
        return img_large, img_name

    def load_mask(self, img_name):
        """Load mask from mask directory with name "img_name".

        Corresponding masks have the same name as their image counterparts, but the format for a mask is .png
        instead of .jpg. If no mask exists, an empty ndarray is created as a placeholder instead.

        :param img_name: (string) name of an image
        :return:
            mask_large: (ndarray) mask
            mask_name: (string) mask name
        """
        mask_name = img_name
        if self.masks_exist:
            mask_filename = mask_name + '.png'
            mask_path = os.path.join(self.masks_dir_path, mask_filename)
            mask_large = cv2.imread(mask_path, 0)
            mask_large = np.where(mask_large > 25, 255, 0)  # binarize
            if np.max(mask_large) > 0:
                mask_large = mask_large / np.max(mask_large)  # normalize
        else:
            mask_large = np.zeros((1, 1))  # Create empty array
        return mask_large, mask_name

    def load_weights(self):
        """Load weights for a model.

        Model is selected with a filedialog.
        """
        logging.info('Select Model weights to evaluate')
        model_path = filedialog.askopenfilename(initialdir='experiments')
        root = tk.Tk()
        root.withdraw()
        logging.info('Selected {}'.format(model_path))
        self.model.load_weights(model_path)

    def pad_image(self, img_large):
        """Create padding for image.

        In order to analyse images, their dimensions must be multiples of the patch size (usually 256). The method for
        padding the image is border reflect.

        :param img_large: (ndarray) original image
        :return:
            img_padded: (ndarray) padded image
        """
        image_large_y = img_large.shape[0]
        image_large_x = img_large.shape[1]
        canvas_y = int(math.ceil(image_large_y / float(self.patch_size))) * self.patch_size
        canvas_x = int(math.ceil(image_large_x / float(self.patch_size))) * self.patch_size
        pd_top = 0
        pd_bot = canvas_y - image_large_y
        pd_lft = 0
        pd_rgt = canvas_x - image_large_x
        img_padded = cv2.copyMakeBorder(img_large, pd_top, pd_bot, pd_lft, pd_rgt, cv2.BORDER_REFLECT)
        return img_padded

    def predict_images(self, type):
        """Predict all images in a folder.

        :param type: (string) type of prediction (color/greyscale images)
        :return:
        """
        logging.info('Start predicting images from: {}'.format(self.img_dir_path))
        # Create Log File
        Logger.create_log_file(self)
        # Iterate through image folder
        for idx, img_filename in enumerate(os.listdir(self.img_dir_path), start=1):  # For all images in folder
            logging.info('Predict: {} / ({}/{})'.format(img_filename, idx, len(os.listdir(self.img_dir_path))))
            # Load image, mask
            img_name = os.path.splitext(img_filename)[0]
            img, img_name = self.load_image(img_name)
            msk, msk_name = self.load_mask(img_name)
            img_padded = self.pad_image(img)
            # Predict single image depending on prediction type
            if type == 'grayscale':
                prd = self.predict_img_greyscale(img_padded)
            elif type == 'color':
                prd = self.predict_img_color(img_padded)
            else:
                types = ['grayscale', 'color']
                raise ValueError("Invalid type. Expected one of: %s" % types)

            # Calculate Metrics
            metrics = [0] * 6
            if self.masks_exist:
                iou = self.calculate_iou(msk, prd)
                cvg_true = self.calculate_coverage(msk)
                cvg_predicted = self.calculate_coverage(prd)
                absolute_cvg_error = abs(cvg_predicted - cvg_true)
                pixel_accuracy = self.calculate_pixel_accuracy(msk, prd)
                pixel_counts = self.calculate_pixel_count(msk, prd)
                metrics = [iou, pixel_accuracy, cvg_true, cvg_predicted, absolute_cvg_error, pixel_counts]

            self.save_prediction(prd, self.predictions_dir_path, file_name=img_filename)  # Save prediction
            self.plot(img, msk, prd, img_filename, metrics)  # Create Plot
            Logger.write_log_line(self, img_filename, metrics)  # Write log-file

    def predict_img_greyscale(self, img):
        """Predict grayscale image.

        :param img: (ndarray) image
        :return:
            prd: (ndarray) prediction
        """
        time_start = time.time()
        # Convert images to grayscale
        img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        # Split the image into small patches
        patches = patchify(img, (self.patch_size, self.patch_size), step=self.patch_size)
        patches_predicted = []
        for i in range(patches.shape[0]):
            for j in range(patches.shape[1]):
                logging.debug('Patch: ', i, j)

                patch_single = patches[i, j, :, :]
                patch_single = patch_single / 255.
                patch_single = np.array(patch_single)
                patch_single = np.stack((patch_single,) * 3, axis=-1)
                patch_single_input = np.expand_dims(patch_single, 0)

                # Predict and threshold for values above 0.5 probability
                single_patch_prediction = (self.model.predict(patch_single_input)[0, :, :, 0] > 0.5).astype(np.uint8)   # Predict
                patches_predicted.append(single_patch_prediction)

        patches_predicted = np.array(patches_predicted)
        patches_predicted_reshaped = np.reshape(patches_predicted,(patches.shape[0], patches.shape[1], self.patch_size, self.patch_size))

        # Stich patches together
        prd = unpatchify(patches_predicted_reshaped, img.shape)
        prd = np.where(prd < 0.5, 0, 1)  # binarize
        time_end = time.time()
        self.time_elapsed = time_end-time_start
        return prd

    def predict_img_color(self, img):
        """Predict color image.

        :param img: (ndarray) image
        :return:
            prd: (ndarray) prediction
        """
        time_start = time.time()
        # Split the image into small images, for 3 color layers
        patches = patchify(img, (self.patch_size, self.patch_size, 3), step=self.patch_size)
        patches = patches[:, :, 0, :, :, :]
        patches_predicted = []
        for i in range(patches.shape[0]):
            for j in range(patches.shape[1]):
                logging.debug('Patch: ', i, j)

                patch_single = patches[i, j, :, :, :]
                patch_single = patch_single / 255.
                patch_single = np.expand_dims(patch_single, axis=0)
                single_patch_prediction = self.model.predict(patch_single)  # Predict
                single_patch_prediction = single_patch_prediction[0, :, :, 0]
                patches_predicted.append(single_patch_prediction)

        patches_predicted = np.array(patches_predicted)
        patches_predicted_reshaped = np.reshape(patches_predicted, [patches.shape[0], patches.shape[1], patches.shape[2], patches.shape[3]])

        # Stich patches together
        patches_predicted_reconstructed = unpatchify(patches_predicted_reshaped, (img.shape[0], img.shape[1]))
        prd = patches_predicted_reconstructed
        prd = prd[0: self.prediction_size_x, 0: self.prediction_size_y]
        prd = np.where(prd < 0.5, 0, 1)   # binarize
        time_end = time.time()
        self.time_elapsed = time_end-time_start
        return prd

    def calculate_iou(self, mask, prediction):
        """Calculate IoU for 2 classes.

        :param mask: (ndarray) mask with values between 0 and 1
        :param prediction: (ndarray) prediction with values between 0 and 1
        :return:
            iou_score_a: (float) Intersection over Union for class a
            iou_score_b: (float) Intersection over Union for class b
        """
        # For Class a (Biofouling)
        mask_a = mask
        prediction_a = prediction
        intersection_a = np.sum(np.logical_and(mask_a, prediction_a))
        union_a = np.sum(np.logical_or(mask_a, prediction_a))
        if union_a > 0:
            iou_score_a = intersection_a / union_a
        else:
            iou_score_a = float("nan")

        # For Class b (Background)
        mask_b = 1.-mask  # Invert 0 and 1
        prediction_b = 1.-prediction  # Invert 0 and 1
        intersection_b = np.sum(np.logical_and(mask_b, prediction_b))
        union_b = np.sum(np.logical_or(mask_b, prediction_b))
        if union_b > 0:
            iou_score_b = intersection_b / union_b
        else:
            iou_score_b = float("nan")

        return iou_score_a, iou_score_b

    def calculate_coverage(self, image):
        """Calculate coverage.

        :param image: (ndarray) image
        :return:
            coverage: (float) coverage
        """
        coverage = (image > 0.1).sum() / (image > -1).sum()
        return coverage

    def calculate_pixel_accuracy(self, mask, prediction):
        """Calculate Pixel Accuracy.

        :param mask: (ndarray) mask
        :param prediction: (ndarray) prediction
        :return:
            pixel accuracy: (float) pixel accuracy
        """
        subtraction = mask-prediction
        pixels_correct = (subtraction == 0.0).sum()
        pixels_total = (subtraction < 500).sum()

        pixel_accuracy = pixels_correct / pixels_total
        return pixel_accuracy

    def calculate_pixel_count(self, mask, prediction):
        """ Count the Classified Pixels.

        Counts classified pixels according to true positive, true negative, false positive, and false negative. Can be
        used for Confusion Matrices.

        :param mask: (float ndarray) mask
        :param prediction: (float ndarray) prediction
        :return:
            pixel count: (int list) [TP,FP,TN,FN]
        """
        # Normalize
        subtraction = mask - prediction
        true_positive = np.sum((subtraction == 0) & (mask == 1))
        false_positive = np.sum(subtraction == -1)
        true_negative = np.sum((subtraction == 0) & (mask == 0))
        false_negative = np.sum(subtraction == 1)

        return [true_positive, false_positive, true_negative, false_negative]

    # Plotting
    def save_prediction(self, prediction, dir_predictions, file_name):
        """ Save prediction.

        :param prediction: (ndarray) prediction
        :param dir_predictions: (string) prediction directory
        :param file_name: (string) name for the plot
        :return:
        """
        path_prediction = os.path.join(dir_predictions, os.path.basename(file_name))
        plt.imsave(path_prediction, prediction)

    def plot(self, image, mask, prediction, file_name, metrics=[0, 0, 0, 0, 0]):
        """ Plot image with prediction.

        :param image: (ndarray) image
        :param mask: (ndarray) mask
        :param prediction: (ndarray) prediction
        :param file_name: (string)
        :param metrics: (list) metrics
        :return:
        """
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        iou, pixel_accuracy, cvg_true, cvg_predicted, cvg_error, pixel_counts = metrics
        iou_a = iou[0]
        iou_b = iou[1]
        iou_mean = (iou_a+iou_b)/2

        # Plot Results
        plt.figure(figsize=(12, 4))
        title = ['Input', 'Ground Truth', 'Prediction']
        plt.subplot(1, 3, 1)
        plt.title(title[0])
        plt.imshow(image, cmap='gray', interpolation='none')
        plt.axis('on')

        plt.subplot(1, 3, 2)
        plt.title(title[1])
        plt.imshow(mask, vmin=0, vmax=1)
        plt.axis('on')

        plt.subplot(1, 3, 3)
        plt.title(title[2])
        plt.imshow(image, cmap='gray', interpolation='none')
        plt.imshow(prediction, cmap='plasma', alpha=0.25, vmin=0, vmax=1)

        # Plot Text if Masks exist
        cvg_true_string = 'Coverage True: {:.2f}%'.format(cvg_true*100)
        cvg_pred_string = 'Coverage Pred: {:.2f}%'.format(cvg_predicted*100)
        cvg_error_string = 'Coverage Error: {:.2f}%'.format(cvg_error*100)
        pa_string = 'Pixel Accuracy: {:.2f}%'.format(pixel_accuracy*100)
        iou_a_string = 'IoUa: {:.2f}%'.format(iou_a*100)
        iou_b_string = 'IoUb: {:.2f}%'.format(iou_b*100)
        iou_m_string = 'IoUm: {:.2f}%'.format(iou_mean*100)

        if iou_a == 0:
            iou_a_string = 'IoUa: nan%'
            iou_m_string = 'IoUm: nan%'
        if iou_b == 0:
            iou_b_string = 'IoUb: nan%'
            iou_m_string = 'IoUm: nan%'

        text = '; '.join([cvg_true_string, cvg_pred_string, cvg_error_string, pa_string, iou_a_string, iou_b_string, iou_m_string])
        plt.figtext(0.125, 1.00, text, fontsize=8, va="top", ha="left")
        plt.axis('on')

        path_prediction = os.path.join(self.predictions_overlay_dir_path, os.path.basename(file_name))
        logging.info('Finished in {:.2f}s | '.format(self.time_elapsed)+text)

        plt.savefig(path_prediction, bbox_inches="tight")
        plt.close('all')


__author__ = 'Christian Donat'
__source__ = ''
