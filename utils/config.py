import json
from dotmap import DotMap
import os
import time
import argparse
import logging


def get_args():
    argparser = argparse.ArgumentParser(description=__doc__)
    argparser.add_argument(
        '-c', '--config',
        dest='config',
        metavar='C',
        default='None',
        help='The Configuration file')
    args = argparser.parse_args()
    return args


def get_config_from_json(json_file):
    """
    Get the config from a json file
    :param json_file:
    :return: config(namespace) or config(dictionary)
    """
    # parse the configurations from the config json file provided
    with open(json_file, 'r') as config_file:
        config_dict = json.load(config_file)

    # convert the dictionary to a namespace using bunch lib
    config = DotMap(config_dict)

    return config, config_dict


def process_config(json_file):
    """ Create experiment directories.

    :param json_file:
    :return: config
    """
    config, _ = get_config_from_json(json_file)
    config.callbacks.tensorboard_log_dir = os.path.join("experiments", config.exp.project, config.exp.name, time.strftime("%Y-%m-%d-%H-%M",time.localtime()), "logs/")
    config.callbacks.checkpoint_dir = os.path.join("experiments", config.exp.project, config.exp.name, time.strftime("%Y-%m-%d-%H-%M",time.localtime()), "checkpoints/")

    # Logging
    logging.basicConfig(level=logging.DEBUG) if config.exp.debug else logging.basicConfig(level=logging.INFO)
    logging.info('Activate Callback') if config.exp.callback else logging.info('Deactivate Callback')

    return config

def create_dirs(dirs):
    """
    dirs - a list of directories to create if these directories are not found
    :param dirs:
    :return exit_code: 0:success -1:failed
    """
    try:
        for dir_ in dirs:
            if not os.path.exists(dir_):
                os.makedirs(dir_)
        return 0
    except Exception as err:
        print("Creating directories error: {0}".format(err))
        exit(-1)