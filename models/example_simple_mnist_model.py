#!/usr/bin/env python

"""
Example Mnist model to test if the framework works
"""

# import
from base.base_model import BaseModel
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
import logging


class Model(BaseModel):
    def __init__(self, config):
        super(Model, self).__init__(config)
        self.build_model()

    def build_model(self):
        logging.info('Build model')
        self.model = Sequential()
        self.model.add(Dense(32, activation='relu', input_shape=(28 * 28,)))
        self.model.add(Dense(16, activation='relu'))
        self.model.add(Dense(10, activation='softmax'))

        self.model.compile(
            loss='sparse_categorical_crossentropy',
            optimizer=self.config.model.optimizer,
            metrics=['acc'],
        )


__author__ = 'Ahmkel'
__source__ = 'https://github.com/Ahmkel/Keras-Project-Template'
