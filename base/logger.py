#!/usr/bin/env python

"""
Class for logger

Class for creating log files and print log lines.
"""

# import
import os


class Logger(object):
    """ This class implements methods for logging metrics."""
    def __init__(self):
        """ This class implements the basic methods for logging.

        Attributes:

        Methods:
        create_log_file(self):
            Create a log file with a header
        write_log_line(self, img_filename, metrics):
            Write a single log line
        """

    def create_log_file(self):
        """Create log file.

         If the file does not exist or replace it if one already exists"""
        log_path = os.path.join(self.img_dir_path, '..', 'log.txt')
        log = open(log_path, "w")
        headline = '{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}'.format('img_filename',
                                                                               'coverage_true',
                                                                               'coverage_predicted',
                                                                               'absolute_coverage_error',
                                                                               'iou_a',
                                                                               'iou_b',
                                                                               'iou_m',
                                                                               'true_positive',
                                                                               'false_positive',
                                                                               'true_negative',
                                                                               'false_negative',
                                                                               'pixel_accuracy',
                                                                               'computation_time'
                                                                               )
        log.write(headline)
        log.close()

    def write_log_line(self, img_filename, metrics=[0, 0, 0, 0, 0, 0]):
        """Write log line for a single image."""
        log_path = os.path.join(self.img_dir_path, '..', 'log.txt')

        iou, pixel_accuracy, cvg_true, cvg_predicted, cvg_error, pixel_counts = metrics
        iou_a = iou[0]
        iou_b = iou[1]
        iou_m = (iou_a + iou_b)/2
        true_positive = pixel_counts[0]
        false_positive = pixel_counts[1]
        true_negative = pixel_counts[2]
        false_negative = pixel_counts[3]

        with open(log_path, 'a') as log:
            string = '\n{}\t{:.5f}\t{:.5f}\t{:.5f}\t{:.5f}\t{:.5f}\t{:.5f}\t{:.1f}\t{:.1f}\t{:.1f}\t{:.1f}\t{:.5f}\t{}'.format(img_filename,
                                                                                                                               cvg_true,
                                                                                                                               cvg_predicted,
                                                                                                                               cvg_error,
                                                                                                                               iou_a,
                                                                                                                               iou_b,
                                                                                                                               iou_m,
                                                                                                                               true_positive,
                                                                                                                               false_positive,
                                                                                                                               true_negative,
                                                                                                                               false_negative,
                                                                                                                               pixel_accuracy,
                                                                                                                               self.time_elapsed)
            log.write(string)
            log.close()


__author__ = 'Christian Donat'
__source__ = ''
