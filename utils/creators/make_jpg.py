"""
Image converter to .jpg.

This Script converts all images in a directory to .jpg by asking for a directory through a file-dialogue.
Copies are created in the same folder with identical filenames and the original images are not deleted.
"""

# imports
import os
from PIL import Image
from tkinter import filedialog

dir = filedialog.askdirectory(initialdir='results')

for file_name in os.listdir(dir):
    file_path = os.path.join(dir, file_name)
    img = Image.open(file_path)
    img_name = os.path.splitext(file_name)[0]
    file_name_new = img_name + '.jpg'
    file_path_new = os.path.join(dir, file_name_new)
    img.save(file_path_new)
