#!/usr/bin/env python

"""
K-means Segmentation

This script performs k-means segmentation on all images in a selected "images" directory.

The user can select the "images" directory through a file-dialog by running this script. In addition, the segmentation
results are validated if a "masks" directory is present on the same level as the "images" directory. The masks are
considered the ground truth. If masks are present, a log file with the performance is created as well.

The images need to be in .jpg and the masks need to be in .png format.

The script is applied both for microfouling and macrofouling.

How to Run:
    python kmeans_evaluator.py
"""

# import
from base.base_evaluator import BaseEval
import cv2
import numpy as np
import logging
import time


# Define Class
class KMeansEvaluator(BaseEval):
    """ This class performs k-means segmentation.

    The selected images are segmented and predictions are created. In order to do this, the images are cut
    into smaller patches, segmented and stitched back together afterwards.

    Methods:
        predict_img_color(self, idx)
            Predicts a single image and returns image, mask and prediction.
    """
    def __init__(self):
        """Initialize KMeansEvaluator.

        Initializes attributes and checks if all necessary directories are present.
        """
        logging.basicConfig(level=logging.INFO)
        logging.info('Create k-means evaluator')
        self.n_classes = 2
        self.patch_size = 256  # Microfouling Patches (256x256)
        #self.patch_size = 200  # Microfouling whole image (1600x1200)
        #self.patch_size = 1000  # Macrofouling (1000x1000)
        self.masks_exist = False

        # Prepare Directories
        self.select_img_dir()
        self.img_dir_path = self.check_dir('images')
        self.masks_dir_path = self.check_dir('masks')
        self.predictions_dir_path = self.check_dir('predictions')
        self.predictions_overlay_dir_path = self.check_dir('predictions overlay')
        self.check_masks()

    def predict_img_color(self, img):
        """Predict a single image.

        :param img: (ndarray) image
        :return:
            prd: (ndarray) prediction
        """
        time_start = time.time()  # Time logging

        # Prediction
        img_reshaped = img.reshape((-1, 3))  # reshape to 2D array of pixels and 3 color values (RGB)
        img_reshaped_float = np.float32(img_reshaped)  # convert to float
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 0.2)  # define stopping criteria

        # Run k-means
        _, labels, (centers) = cv2.kmeans(img_reshaped_float,
                                          self.n_classes,
                                          None,
                                          criteria,
                                          10,
                                          cv2.KMEANS_RANDOM_CENTERS)

        # Convert Prediction
        centers = np.uint8(centers)  # convert back to 8 bit values
        labels = labels.flatten()  # flatten the labels array
        prd = centers[labels.flatten()]  # convert all pixels to the color of the centroids
        prd = prd.reshape(img.shape)  # reshape back to the prior image dimension
        prd = cv2.cvtColor(prd, cv2.COLOR_RGB2GRAY)  # convert to greyscale
        prd = np.where(prd < prd.max(), 255, 0)  # binarize
        if np.max(prd) > 0:
            prd = prd/np.max(prd)

        time_end = time.time()  # Stop Time logging
        self.time_elapsed = time_end-time_start  # Estimate Running Time
        return prd


# Run Segmentation
kmeans = KMeansEvaluator()
kmeans.predict_images(type='color')

__author__ = 'Abdou Rockikz'
__source__ = 'https://www.thepythoncode.com/article/kmeans-for-image-segmentation-opencv-python'
