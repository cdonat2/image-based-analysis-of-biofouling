#!/usr/bin/env python

"""
Script to run Tensorboard
"""

import os

os.system('tensorboard --logdir=../experiments/ --host localhost --port 8088')

__author__ = 'Christian Donat'
__source__ = ''

