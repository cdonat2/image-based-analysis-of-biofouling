#!/usr/bin/env python

"""
Trainer for segmentation with transfer learning
Defines Trainer and Callbacks
"""

# import
from base.base_trainer import BaseTrain
import os
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard
from utils.config import create_dirs
import logging


class Trainer(BaseTrain):
    def __init__(self, model, data, config):
        super(Trainer, self).__init__(model, data, config)
        logging.info('Create trainer')
        self.callbacks = []
        self.epochs = config.trainer.num_epochs
        #self.config = config

        if config.exp.callback:
            create_dirs([config.callbacks.tensorboard_log_dir, config.callbacks.checkpoint_dir])
            self.init_callbacks()

    def init_callbacks(self):
        self.callbacks.append(
            ModelCheckpoint(
                filepath=os.path.join(self.config.callbacks.checkpoint_dir, '%s-{epoch:02d}-{val_loss:.2f}.hdf5' % self.config.exp.name),
                monitor=self.config.callbacks.checkpoint_monitor,
                mode=self.config.callbacks.checkpoint_mode,
                save_best_only=self.config.callbacks.checkpoint_save_best_only,
                save_weights_only=self.config.callbacks.checkpoint_save_weights_only,
                verbose=self.config.callbacks.checkpoint_verbose,
            )
        )

        self.callbacks.append(
            TensorBoard(
                log_dir=self.config.callbacks.tensorboard_log_dir,
                write_graph=self.config.callbacks.tensorboard_write_graph,
                profile_batch=0,
                update_freq='batch'
            )
        )

    def train(self):
        logging.info('Start training the model.')

        size_training = self.config.trainer.training_size
        size_validation = self.config.trainer.validation_size
        steps_per_epoch = size_training // self.config.trainer.batch_size
        validation_steps = size_validation // self.config.trainer.batch_size
        # https://stackoverflow.com/questions/45943675/meaning-of-validation-steps-in-keras-sequential-fit-generator-parameter-list

        self.model.fit(
            self.data['train'],
            validation_data=self.data['val'],
            steps_per_epoch=steps_per_epoch,
            validation_steps=validation_steps,
            epochs=self.epochs,
            callbacks=self.callbacks
        )


__author__ = 'Google'
__source__ = 'https://www.tensorflow.org/tutorials/images/segmentation'
