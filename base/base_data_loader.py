#!/usr/bin/env python

"""
Base class for Data Loader

This module implements the basic Data Loader class.

This module contains basic methods to create datasets.

Classes:
    BaseDataLoader
"""


class BaseDataLoader(object):
    """ This class implements the basic methods for data handling.

    Attributes:
          config (file) config information

    Methods:
        def get_data(self):
            get dataset for training
    """
    def __init__(self, config):
        """ Initialize BaseDataLoader.

        Load config.

        :param config: (json) config file for experiment
        """
        self.config = config

    def get_data(self):
        """ Get dataset for training.

        :return: self.dataset
        """
        raise NotImplementedError


__author__ = 'Ahmkel'
__source__ = 'https://github.com/Ahmkel/Keras-Project-Template'
