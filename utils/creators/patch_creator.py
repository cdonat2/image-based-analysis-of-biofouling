#!/usr/bin/env python

"""
This script creates patches from an image dataset.

This Scripts takes images and masks from a folder, separates them into patches and saves them as patches
with size 256x256.

It requires python3.7 to run, in order to be able to use the patchify packages.
"""

# Imports
import numpy as np
import os
from pathlib import Path
import cv2
from patchify import patchify
from matplotlib import pyplot as plt
import logging
from tkinter import filedialog

# Specify Path
dataset_dir = filedialog.askdirectory(initialdir='results')

#logging.basicConfig(level=logging.INFO)
logging.basicConfig(level=logging.DEBUG)

logging.info('Taking images from {}'.format(dataset_dir))

# Images
for root, dir, files in os.walk(os.path.join(dataset_dir, 'images'), topdown=True):
    logging.debug(root)
    for img_path in files:
        logging.debug(img_path)

        img_large = cv2.imread(os.path.join(root, img_path), 1)
        img_large = cv2.cvtColor(img_large, cv2.COLOR_BGR2RGB)
        img_height, img_width, img_channels = img_large.shape

        # Sanity Check
        plt.imshow(img_large)
        plt.show()

        # Patchify
        if img_height > 256 and img_width > 256:
            img_patches = patchify(img_large, (256, 256, 3), step=256)  # Step=256 for 256 patches means no overlap
            for i in range(img_patches.shape[0]):
                for j in range(img_patches.shape[1]):
                    img_single_patch = img_patches[i, j, :, :]
                    img_single_patch = img_single_patch[0]

                    # Sanity Check
                    plt.imshow(img_single_patch)
                    plt.show()

                    img_name = Path(img_path).stem
                    save_path = os.path.join(dataset_dir, 'patches', 'images', img_name + str(i) + str(j) + '.jpg')
                    cv2.imwrite(save_path, cv2.cvtColor(img_single_patch, cv2.COLOR_RGB2BGR))
        else:
            logging.info('Image to small with {}x{}'.format(img_width, img_height))

logging.info('Finished with Images')

# Masks
for root, dir, files in os.walk(os.path.join(dataset_dir, 'masks'), topdown=True):
    logging.debug(root)
    for img_path in files:
        logging.debug(img_path)

        img_large = cv2.imread(os.path.join(root, img_path), 0)
        img_large = cv2.cvtColor(img_large, cv2.COLOR_BGR2RGB)
        img_height, img_width, img_channels = img_large.shape

        # Sanity Check
        #plt.imshow(img_large)
        #plt.show()

        # Patchify
        if img_height > 256 and img_width > 256:
            img_patches = patchify(img_large, (256, 256, 3), step=256)  # Step=256 for 256 patches means no overlap
            for i in range(img_patches.shape[0]):
                for j in range(img_patches.shape[1]):
                    img_single_patch = img_patches[i, j, :, :]
                    img_single_patch = img_single_patch[0]

                    # Sanity Check
                    #plt.imshow(img_single_patch)
                    #plt.show()

                    img_single_patch = np.where(img_single_patch > 35, 255, 0)
                    img_name = Path(img_path).stem
                    save_path = os.path.join(dataset_dir, 'patches', 'masks', img_name + str(i) + str(j) + '.png')
                    cv2.imwrite(save_path, img_single_patch)
        else:
            logging.debug('Image to small with {}x{}'.format(img_width, img_height))

logging.info('Finished with Masks')

__author__ = "digitalsreeni"
__source__ = "https://www.youtube.com/watch?v=7IL7LKSLb9I"