#!/usr/bin/env python

"""
Base class for model
"""


class BaseModel(object):
    def __init__(self, config):
        self.config = config
        self.model = None

    def build_model(self):
        raise NotImplementedError


__author__ = 'Ahmkel'
__source__ = 'https://github.com/Ahmkel/Keras-Project-Template'
