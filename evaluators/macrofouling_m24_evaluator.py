#!/usr/bin/env python

"""
Evaluator for barnacle transfer segmentation

This class includes methods to calculate general statistics and predict images.
"""

# import
from base.base_evaluator import BaseEval
import logging


class Evaluator(BaseEval):
    def __init__(self, model, data, config):
        super(Evaluator, self).__init__(model, data, config)
        logging.info('Create evaluator')
        self.model = model
        self.data = data
        self.config = config

    def eval(self):
        logging.info('Evaluate model')
        score = self.model.evaluate(self.data['val'].take(100))
        logging.info(f'Jaccard Loss: {score[0]} / IoU Score: {score[1]}')


__author__ = 'Google'
__source__ = 'https://www.tensorflow.org/tutorials/images/segmentation'
