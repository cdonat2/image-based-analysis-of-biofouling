#!/usr/bin/env python

"""
Base class for plotting
"""

# import
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.ticker as mtick


class BasePlotter(object):
    """ This class implements methods for plotting diagrams."""
    def __init__(self):
        """
        This class implements the methods for plotting.
        """
        self.save = True
        self.scale = 0
        self.x_range = [0, 0]
        self.y_range = [0, 0]

        tex_fonts = {
            # Use LaTeX to write all text
            "text.usetex": True,
            "font.family": "serif",
            # Use 10pt font in plots, to match 10pt font in document
            "axes.labelsize": 10,
            "font.size": 10,
            # Make the legend/label fonts a little smaller
            "legend.fontsize": 8,
            "xtick.labelsize": 8,
            "ytick.labelsize": 8
        }

        plt.rcParams.update(tex_fonts)

    def create_dataframe(self, path, experiment):
        """
        Create a dataframe from .csv data
        :param path:
        :param experiment:
        :return:
        """
        df = pd.read_csv(path, sep="\t")
        df = df.assign(experiment=[experiment] * len(df.index))
        measurement_error = df.coverage_predicted - df.coverage_true
        df = df.assign(measurement_error=measurement_error)
        return df

    def set_format(self, ax):
        """
        Set format for plotting
        :param ax:
        :return:
        """
        # Formatting
        # Grid Lines
        plt.grid(which='major', color='#666666', linestyle='-', alpha=0.5, axis='x')
        plt.grid(which='minor', color='#999999', linestyle='-', alpha=0.3, axis='x')

        # Axis
        plt.minorticks_on()
        ax.set_axisbelow(True)
        ax.tick_params(which='major', direction='in')
        ax.tick_params(which='minor', direction='in')
        ax.set_xlabel('Method')

    def microfouling_overview(self, labels, scores, title, filename):
        """ Single Bar Plot for Microfouling.

        :return:
        """

        fig, ax = plt.subplots(1, 1, figsize=[3, 2.95])  # 2 Diagrams

        x_pos = np.arange(len(labels))
        bar_width = 0.8

        handles = plt.bar(x_pos, scores, width=bar_width, color='white', edgecolor='black', hatch=['//', '..', '---'])

        # Create names on the x-axis
        plt.xticks(x_pos, labels)

        # Formatting
        # Grid Lines
        plt.grid(which='major', color='#666666', linestyle='-', alpha=0.3, axis='y')
        plt.grid(which='minor', color='#999999', linestyle='-', alpha=0.15, axis='y')

        # Axis
        plt.minorticks_on()
        ax.set_axisbelow(True)
        ax.tick_params(which='major', direction='in')
        ax.tick_params(which='minor', direction='in')
        ax.set_ylim(0, self.scale)

        # Annotation
        ax.set_xlabel('Method')
        ax.set_ylabel(title)
        ax.set_title(title)

        ax.set_xticks(x_pos)
        ax.set_xticklabels(labels)
        ax.legend(handles, labels, loc='best')

        # Percentage above bars
        for idx, score in enumerate(scores):
            annotation = "{0:0.2f}\%".format(score*100)
            plt.text(x_pos[idx]-bar_width/3.0, score + 0.02*max(scores), annotation)

        # Show graph
        fig.tight_layout()
        plt.show()

        # Save figure
        if self.save:
            fig.savefig(filename+'.pdf', format='pdf', bbox_inches='tight')

    def macrofouling_overview(self, labels, scores, title, filename):
        """ Single Bar Plot for Microfouling.

        :return:
        """

        #fig, ax = plt.subplots(1, 1, figsize=[6, 2.95])  # 1 Diagram
        fig, ax = plt.subplots(1, 1, figsize=[3.35, 2.95])  # 2 Diagrams

        x_pos = np.arange(len(labels))
        bar_width = 0.8

        # Create Bars
        handles = plt.bar(x_pos, scores, color='white', edgecolor='black', hatch=['//', '..', '---', 'xxx', '++++'])

        # Create names on the x-axis
        plt.xticks(x_pos, labels)

        # Formatting
        # Grid Lines
        plt.grid(which='major', color='#666666', linestyle='-', alpha=0.3, axis='y')
        plt.grid(which='minor', color='#999999', linestyle='-', alpha=0.15, axis='y')

        # Axis
        plt.minorticks_on()
        ax.set_axisbelow(True)
        ax.tick_params(which='major', direction='in')
        ax.tick_params(which='minor', direction='in')
        ax.set_ylim(0, self.scale)

        # Annotation
        ax.set_xlabel('Method')
        ax.set_ylabel(title)
        ax.set_title(title)

        ax.set_xticks(x_pos)
        ax.set_xticklabels(labels)
        ax.legend(handles, labels, loc='best')

        # Percentage above bars
        for idx, score in enumerate(scores):
            annotation = "{0:0.2f}\%".format(score*100)
            plt.text(x_pos[idx]-bar_width/1.8, score + 0.02*max(scores), annotation)

        # Show graph
        fig.tight_layout()
        plt.show()

        # Save figure
        if self.save:
            fig.savefig(filename+'.pdf', format='pdf', bbox_inches='tight')

    def precision_recall(self, labels, precision, recall, filename):
        """ Single Bar Plot for Microfouling.

        :return:
        """

        # fig, ax = plt.subplots(1, 1, figsize=[6, 2.95])  # 1 Diagram
        fig, ax = plt.subplots(1, 1, figsize=[3, 2.95])  # 2 Diagrams
        bar_width = 0.4

        # The x position of bars
        x1 = np.arange(len(precision))
        x2 = [x + bar_width for x in x1]

        plt.bar(x1, precision, width=bar_width, color='white', edgecolor='black', hatch=['//'], label=labels[0])
        plt.bar(x2, recall, width=bar_width, color='white', edgecolor='black', hatch=['..'], label=labels[1])

        # general layout
        #plt.xticks([r + 0.5 * bar_width for r in range(len(precision))], ['Biofouling', 'Background'])

        plt.legend()

        # Formatting
        # Grid Lines
        plt.grid(which='major', color='#666666', linestyle='-', alpha=0.3, axis='y')
        plt.grid(which='minor', color='#999999', linestyle='-', alpha=0.15, axis='y')

        # Axis
        plt.minorticks_on()
        ax.set_axisbelow(True)
        ax.tick_params(which='major', direction='in')
        ax.tick_params(which='minor', direction='in')
        ax.set_ylim(0, self.scale)

        # Annotation
        ax.set_xlabel('Method')

        # ax.set_ylabel('Mean IoU')
        ax.set_ylabel('Precision / Recall')

        ax.set_title('Precision / Recall')

        ax.set_xticks(x2)
        for idx, score in enumerate(precision):
            annotation = "{0:0.2f}\%".format(score*100)
            plt.text(x1[idx]-0.175, score + 0.02, annotation)

        for idx, score in enumerate(recall):
            annotation = "{0:0.2f}\%".format(score*100)
            plt.text(x2[idx]-0.175, score + 0.02, annotation)

        # Create names on the x-axis
        plt.xticks(x1 + bar_width/2, ['Biofouling', 'Background'])

        # Show graph
        fig.tight_layout()
        plt.show()

        # Save figure
        if self.save:
            fig.savefig(filename+'.pdf', format='pdf', bbox_inches='tight')

    def plot_bar_chart_macrofouling_monthly(self, labels, kmeans, m23, m24, m234, m2349, filename):
        """ Single Bar Plot for Microfouling.

        :return:
        """

        fig, ax = plt.subplots(1, 1, figsize=[6, 2.95])  # 1 Diagram
        #fig, ax = plt.subplots(1, 1, figsize=[3, 2.95])  # 2 Diagrams
        bar_width = 0.15

        # The x position of bars
        x1 = np.arange(len(kmeans))
        x2 = [x + bar_width for x in x1]
        x3 = [x + 2 * bar_width for x in x1]
        x4 = [x + 3 * bar_width for x in x1]
        x5 = [x + 4 * bar_width for x in x1]

        plt.bar(x1, kmeans, width=bar_width, color='white', edgecolor='black', hatch=['////'], label=labels[0])
        plt.bar(x2, m23, width=bar_width, color='white', edgecolor='black', hatch=['++++'], label=labels[1])
        plt.bar(x3, m24, width=bar_width, color='white', edgecolor='black', hatch=['....'], label=labels[2])
        plt.bar(x4, m234, width=bar_width, color='white', edgecolor='black', hatch=['----'], label=labels[3])
        plt.bar(x5, m2349, width=bar_width, color='white', edgecolor='black', hatch=['xxxx'], label=labels[4])


        # general layout
        #plt.xticks([r + 0.5 * bar_width for r in range(len(precision))], ['Biofouling', 'Background'])

        plt.legend()

        # Formatting
        # Grid Lines
        plt.grid(which='major', color='#666666', linestyle='-', alpha=0.3, axis='y')
        plt.grid(which='minor', color='#999999', linestyle='-', alpha=0.15, axis='y')

        # Axis
        plt.minorticks_on()
        ax.set_axisbelow(True)
        ax.tick_params(which='major', direction='in')
        ax.tick_params(which='minor', direction='in')
        ax.set_ylim(0, 1.0)

        # Annotation
        ax.set_xlabel('Month')

        # ax.set_ylabel('Mean IoU')
        ax.set_ylabel('mIoU')

        ax.set_title('Monthly Overview')

        ax.set_xticks(x2)

        # Create names on the x-axis
        plt.xticks(x1 + bar_width/2, ['M5', 'M7', 'M8', 'M10', 'M11', 'M12'])

        # Show graph
        fig.tight_layout()
        plt.show()

        # Save figure
        if self.save:
            fig.savefig(filename+'.pdf', format='pdf', bbox_inches='tight')

    def plot_confusion_matrix(self, counts, title, filename):
        """Plot confusion matrix using heatmap.

        Args:
            counts (ndarray): counts
        """
        fig, ax = plt.subplots(1, 1, figsize=[3, 2.95])

        group_names = ['True Pos', 'False Pos', 'False Neg', 'True Neg']
        group_counts = ['{0:0.0f}'.format(value) for value in counts.flatten()]
        group_percentages = ['{0:0.2f}\%'.format(value * 100) for value in counts.flatten() / np.sum(counts)]

        labels = [f'{v1}\n{v2}\n{v3}' for v1, v2, v3 in zip(group_names, group_counts, group_percentages)]
        labels = np.asarray(labels).reshape(2, 2)
        #categories = ['Biofouling', 'Background']
        categories = ['Biofouling', 'Background']

        sns.heatmap(counts, annot=labels, fmt='', cmap='viridis', vmax=self.scale)

        ax.set(ylabel="Predicted Label", xlabel="True Label")
        #ax.set_title(title)

        ax.xaxis.set_ticklabels(categories)
        ax.yaxis.set_ticklabels(categories)

        plt.show()

        # Save figure
        if self.save:
            fig.savefig(filename+'.pdf', format='pdf', bbox_inches='tight')

    def plot_violing_horizontal(self, order, labels, df, title,filename):
        """
        Plot horizontal violing plot (Prediction Bias)
        :param order:
        :param labels:
        :param df:
        :param title:
        :param filename:
        :return:
        """

        df2 = df.append([-999, -999, -999, -999, 'dummy'])
        df2['huecol'] = False
        df2['huecol'].iloc[-1] = True

        # plot
        fig, ax = plt.subplots(1, 1, figsize=[6, 2.95])
        sns.violinplot(y='experiment',
                       x="measurement_error",
                       split=True,
                       hue='huecol',
                       inner='box',
                       orient='h',
                       color="lightgray",
                       cut=0,
                       data=df2,
                       linewidth=1.0,
                       legend=False,
                       order=order,
                       hue_order=[True, False]
                       )

        # Formatting
        self.set_format(ax)
        ax.set_title(title)
        ax.set_ylabel('Method')
        ax.set_xlabel('Prediction Bias')
        ax.set_yticklabels(labels)
        ax.xaxis.set_major_formatter(mtick.PercentFormatter(1.0))
        ax.invert_yaxis()

        # remove hue legend
        leg = plt.gca().legend()
        leg.remove()
        plt.ylim(self.y_range)
        plt.xlim(self.x_range)

        plt.show()
        fig.savefig(filename + '.pdf', format='pdf', bbox_inches='tight')


__author__ = 'Christian Donat'
__source__ = ''
