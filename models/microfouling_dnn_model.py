#!/usr/bin/env python

"""
    Model for microfouling segmentation with convolutional neural networks.
"""

# import
from base.base_model import BaseModel
import segmentation_models as sm
import logging


class Model(BaseModel):
    def __init__(self, config):
        super(Model, self).__init__(config)
        self.build_model()

    def build_model(self):
        logging.info('Build model')
        BACKBONE = 'resnet34'

        # define model
        self.model = sm.Unet(BACKBONE, encoder_weights='imagenet')
        self.model.compile('Adam', loss=sm.losses.bce_jaccard_loss, metrics=[sm.metrics.iou_score])
        #print(self.model.summary())


__author__ = 'Dr. Sreenivas Bhattiprolu'
__source__ = 'https://github.com/bnsreenu/python_for_microscopists/blob/master/216_mito_unet_12_training_images_V1.0.py'
