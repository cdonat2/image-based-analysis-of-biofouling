#!/usr/bin/env python

"""
    Data loader for microfouling segmentation with convolutional neural networks.
"""

# import
from base.base_data_loader import BaseDataLoader
import os
import numpy as np
import matplotlib.pyplot as plt
import segmentation_models as sm
import glob
import cv2
import random
import logging


class DataLoader(BaseDataLoader):
    def __init__(self, config):
        super(DataLoader, self).__init__(config)
        logging.info('Create data loader')
        self.SIZE_Y = config.model.img_height
        self.SIZE_X = config.model.img_width
        self.n_classes = config.model.num_cls
        self.seed = config.exp.seed
        self.environment = 'windows'
        sm.set_framework('tf.keras')
        sm.framework()

        # Select Folder:
        dir_images = os.path.join('datasets', '01 Dublin', '01 Microfouling', '02 Preprocessed', 'C13', '01 Images Combined', 'training and validation', 'images')
        dir_masks = os.path.join('datasets', '01 Dublin', '01 Microfouling', '02 Preprocessed', 'C13', '01 Images Combined', 'training and validation', 'masks')

        # Capture training image info as a list
        self.train_images = []
        for directory_path in glob.glob(dir_images):
            for img_path in sorted(glob.glob(os.path.join(directory_path, '*.jpg'))):
                img = cv2.imread(img_path, 0)
                img = cv2.resize(img, (self.SIZE_Y, self.SIZE_X))
                img = img / 255.
                self.train_images.append(img)

        # Convert list to array for machine learning processing
        self.train_images = np.array(self.train_images)
        self.train_images = np.stack((self.train_images,)*3, axis=-1)

        logging.info('Taking images from: {}'.format(dir_images))
        logging.info('Found {} images '.format(np.shape(self.train_images)[0]))

        # Capture mask/label info as a list
        self.train_masks = []
        for directory_path in glob.glob(dir_masks):
            for mask_path in sorted(glob.glob(os.path.join(directory_path, '*.png'))):
                mask = cv2.imread(mask_path, 0)
                mask = cv2.resize(mask, (self.SIZE_Y, self.SIZE_X), interpolation=cv2.INTER_NEAREST)  # Otherwise ground truth changes due to interpolation
                mask = mask / 255.
                self.train_masks.append(mask)

        # Convert list to array for machine learning processing
        self.train_masks = np.array(self.train_masks)
        self.train_masks = np.stack((self.train_masks,)*3, axis=-1)

        logging.info('Taking masks from: {}'.format(dir_masks))
        logging.info('Found {} images '.format(np.shape(self.train_masks)[0]))

        # Define the model
        BACKBONE = 'resnet34'
        preprocess_input1 = sm.get_preprocessing(BACKBONE)

        # preprocess input
        images1 = preprocess_input1(self.train_images)

        from sklearn.model_selection import train_test_split
        X_train, self.X_test, y_train, self.y_test = train_test_split(images1, self.train_masks, test_size=0.25, random_state=42)

        # New generator with rotation and shear where interpolation that comes with rotation and shear are thresholded in masks.
        # This gives a binary mask rather than a mask with interpolated values.
        from tensorflow.keras.preprocessing.image import ImageDataGenerator

        img_data_gen_args = dict(rotation_range=90,
                                 width_shift_range=0.3,
                                 height_shift_range=0.3,
                                 shear_range=0.5,
                                 zoom_range=0.3,
                                 horizontal_flip=True,
                                 vertical_flip=True,
                                 fill_mode='reflect')

        mask_data_gen_args = dict(rotation_range=90,
                                  width_shift_range=0.3,
                                  height_shift_range=0.3,
                                  shear_range=0.5,
                                  zoom_range=0.3,
                                  horizontal_flip=True,
                                  vertical_flip=True,
                                  fill_mode='reflect',
                                  preprocessing_function=lambda x: np.where(x > 0, 1, 0).astype(x.dtype))  # Binarize the output again.

        image_data_generator = ImageDataGenerator(**img_data_gen_args)
        image_data_generator.fit(X_train, augment=True, seed=self.seed)

        image_generator = image_data_generator.flow(X_train, seed=self.seed)
        valid_img_generator = image_data_generator.flow(self.X_test, seed=self.seed)

        mask_data_generator = ImageDataGenerator(**mask_data_gen_args)
        mask_data_generator.fit(y_train, augment=True, seed=self.seed)
        mask_generator = mask_data_generator.flow(y_train, seed=self.seed)
        valid_mask_generator = mask_data_generator.flow(self.y_test, seed=self.seed)

        def my_image_mask_generator(image_generator, mask_generator):
            train_generator = zip(image_generator, mask_generator)
            for (img, mask) in train_generator:
                yield (img, mask)

        self.my_generator = my_image_mask_generator(image_generator, mask_generator)
        self.validation_datagen = my_image_mask_generator(valid_img_generator, valid_mask_generator)

    def get_data(self):
        return self.my_generator, self.validation_datagen, self.X_test, self.y_test

__author__ = 'Adapted from Dr. Sreenivas Bhattiprolu'
__source__ = 'https://github.com/bnsreenu/python_for_microscopists/blob/master/216_mito_unet_12_training_images_V1.0.py'
