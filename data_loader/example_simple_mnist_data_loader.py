#!/usr/bin/env python

"""
Data loader for Mnist example
"""

# import
from base.base_data_loader import BaseDataLoader
from tensorflow.keras.datasets import mnist
import logging


class DataLoader(BaseDataLoader):
    def __init__(self, config):
        super(DataLoader, self).__init__(config)
        logging.info('Create data loader')
        (self.X_train, self.y_train), (self.X_test, self.y_test) = mnist.load_data()
        self.X_train = self.X_train.reshape((-1, 28 * 28))
        self.X_test = self.X_test.reshape((-1, 28 * 28))
        self.data = self.X_train, self.y_train, self.X_test, self.y_test

    def get_data(self):
        return self.data


__author__ = 'Adapted from Ahmkel'
__source__ = 'https://github.com/Ahmkel/Keras-Project-Template'
